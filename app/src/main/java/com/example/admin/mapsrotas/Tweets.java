package com.example.admin.mapsrotas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Tweets extends AppCompatActivity {

    public WebView tweets;
    String destino;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweets);
        destino = getIntent().getStringExtra("destino");

        tweets = (WebView) findViewById(R.id.tweets);
        tweets.getSettings().setJavaScriptEnabled(true);
        tweets.loadUrl("https://mobile.twitter.com/search/"+destino);
    }
}
