package com.example.admin.mapsrotas;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class Rotas extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String origem;
    String destino;
    LatLng city_origin;
    LatLng city_destiny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotas);
        Intent intent = getIntent();
        origem = intent.getStringExtra("origem");
        destino = intent.getStringExtra("destino");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        switch(origem){
            case "Sobral":
                city_origin = new LatLng(-3.6836717,-40.3856141);
                break;
            case "Jericoacoara":
                city_origin = new LatLng( -2.7959301,-40.5165467);
                break;
            case "Fortaleza":
                city_origin = new LatLng( -3.7905171,-38.5885078);
                break;
            case "Ibiapina":
                city_origin = new LatLng( -3.9256447,-40.8981307);
                break;

            case "Hidrolândia":
                city_origin = new LatLng(-4.4565256,-40.556281);
                break;

            case "Piripiri":
                city_origin = new LatLng(-4.2776849,-41.8103338);
                break;

            case "Ipaporanga":
                city_origin = new LatLng(-4.9031081,-40.7690448);
                break;

            case "Teresina":
                city_origin = new LatLng(-5.1856982,-43.0816732);
                break;

            case "Camocim":
                city_origin = new LatLng(-2.9654254,-40.9475567);
                break;

            case "Itapipoca":
                city_origin = new LatLng(-3.494544,-39.599571);
                break;

        }

        switch(destino){
            case "Sobral":
                city_destiny = new LatLng(-3.6836717,-40.3856141);
                break;
            case "Jericoacoara":
                city_destiny = new LatLng( -2.7959301,-40.5165467);
                break;
            case "Fortaleza":
                city_destiny = new LatLng( -3.7905171,-38.5885078);
                break;
            case "Ibiapina":
                city_destiny = new LatLng( -3.9256447,-40.8981307);
                break;

            case "Hidrolândia":
                city_destiny = new LatLng(-4.4565256,-40.556281);
                break;

            case "Piripiri":
                city_destiny = new LatLng(-4.2776849,-41.8103338);
                break;

            case "Ipaporanga":
                city_destiny = new LatLng(-4.9031081,-40.7690448);
                break;

            case "Teresina":
                city_destiny = new LatLng(-5.1856982,-43.0816732);
                break;

            case "Camocim":
                city_destiny = new LatLng(-2.9654254,-40.9475567);
                break;

            case "Itapipoca":
                city_destiny = new LatLng(-3.494544,-39.599571);
                break;
        }

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(city_origin, 2));
        googleMap.addMarker(new MarkerOptions().title(origem).position(city_origin));
        googleMap.addMarker(new MarkerOptions().title(destino).position(city_destiny));
        googleMap.addPolyline(new PolylineOptions().geodesic(true).add(city_origin).add(city_destiny));

    }
}
