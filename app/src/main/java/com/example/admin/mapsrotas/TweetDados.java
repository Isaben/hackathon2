package com.example.admin.mapsrotas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class TweetDados extends AppCompatActivity {

    public WebView tweet;
    public WebView dados;
    String destino;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        destino = getIntent().getStringExtra("destino");
        tweet = (WebView) findViewById(R.id.webView1);
        dados = (WebView) findViewById(R.id.webView2);
        tweet.getSettings().setJavaScriptEnabled(true);
        tweet.loadUrl("https://pt.m.wikipedia.org/wiki/"+destino);

        dados.getSettings().setJavaScriptEnabled(true);
        dados.loadUrl("https://mobile.twitter.com/search/"+destino);
    }
}
