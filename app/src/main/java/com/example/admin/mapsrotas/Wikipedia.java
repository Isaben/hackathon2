package com.example.admin.mapsrotas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Wikipedia extends AppCompatActivity {

    public WebView wikipedia;
    String destino;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wikipedia);
        destino = getIntent().getStringExtra("destino");

        wikipedia = (WebView) findViewById(R.id.wikipedia);
        wikipedia.getSettings().setJavaScriptEnabled(true);
        wikipedia.loadUrl("https://pt.m.wikipedia.org/wiki/"+destino);
    }
}
