package com.example.admin.mapsrotas;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner spinner1;
    Spinner spinner2;
    ArrayList<String> cidades;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);

        cidades = new ArrayList<>();
        cidades.add("Sobral");
        cidades.add("Jericoacoara");
        cidades.add("Teresina");
        cidades.add("Ibiapina");
        cidades.add("Hidrolândia");
        cidades.add("Piripiri");
        cidades.add("Ipaporanga");
        cidades.add("Fortaleza");
        cidades.add("Itapipoca");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cidades);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);
    }

    public void criar_rota(View view){
        String origem = spinner1.getSelectedItem().toString();
        String destino = spinner2.getSelectedItem().toString();

        Intent intent = new Intent(this, Rotas.class);
        intent.putExtra("origem", origem);

        intent.putExtra("destino", destino);
        startActivity(intent);
    }

    public void criar_tweets(View v){
        String destino = spinner2.getSelectedItem().toString();
        Intent intent = new Intent(this, Tweets.class);
        intent.putExtra("destino", destino);
        startActivity(intent);

    }

    public void criar_dados(View v){
        String destino = spinner2.getSelectedItem().toString();
        Intent intent = new Intent(this, Wikipedia.class);
        intent.putExtra("destino", destino);
        startActivity(intent);

    }

    public void criar_busca(View viwe){
        String destino = spinner2.getSelectedItem().toString();

        Intent intent = new Intent(this, TweetDados.class);
        intent.putExtra("destino", destino);
        startActivity(intent);
    }
}
